// Opens new window
const analyticalLab = () => {
    window.open("https://digital.buhlergroup.com/grainigo/");
};

const aspirationChannel = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/aspiration_channel.html");
};

const aspirationSystem = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/process-technologies/Handling/Industrial-dust-extraction-systems.html");
};

const autoHopperScale = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/tubex_pro_automatichopperscale.html");
};

const bid = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/key-topics/Digitalization/Digital-platform.html");
};

const conveyorDryer = () => {
    window.open("buhlergroup.com/content/buhlergroup/global/en/products/ceres_conveyor_dryer.html");
};

const degerimantor = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/degerminator.html");
};

const dischargeStation = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/discharge_station.html");
};

const drumGraderRoto = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/rotosort_drum_grader.html");
};

const dustSC = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/process-technologies/Handling/Industrial-dust-extraction-systems.html");
};

const fineGrind = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/multimpactfine_hammermill.html");
}

const flakerAndRoll = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/industries/Extrusion-solutions/Breakfast-Cereals.html");
};

const gravFeeders = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/micro_differentialproportioningscale.html");
};

const highCapGrainCleaner = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/vega_grain_classifier.html");
};

const impactDehuller = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/huller.html");
};

const intakeStation2 = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/discharge_station.html");
};

const limeAddition = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/industries/Wheat-and-grain/Corn.html");
};

const mercury = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/mercury_mes_manufacturingexecutionsystem.html");
};

const opitcalSorter = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/sortex_a_opticalsorter.html");
};

const pearlerTop = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/topwhite_verticalwhitener.html");
};

const pinMill = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/impact_machine.html");
};

const planSifters = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/seginus_small_plansifter.html");
};

const preconditioner = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/extruder.html");
};

const psmu = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/particle_size_measurementunit.html");
};

const purifier = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/puromat_purifier.html");
};

const rollerMill = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/diorit_mill.html");
};

const rollerMill2 = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/mill.html");
};

const steamCond = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/steamer.html");
};

const temperingSystem = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/moisture_controlunit.html");
};

const tubularPush = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/industries/Baked-goods/bakery-solutions.html");
};

const twinScrew = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/extruder.html");
};

const twoStage = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/extruder.html");
}

const vibrosifter = () => {
    window.open("https://www.buhlergroup.com/content/buhlergroup/global/en/products/vibro_sieving_machine.html");
};

// Opens mail client

const openMail = () => {
    window.location.href = "mailto:buhler.minneapolis@buhlergroup.com?subject=360 Guided Tour Inquiry";
};

